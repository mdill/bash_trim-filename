# BASH trim filename

## Purpose

Both `trim-front` and `trim-end` are BASH scripts which will accept an argument
as an integer.  It will then "trim" that number of characters from either the
front, or back, of all of the filenames in the directory the script was run.
If `trim-end` is called, a Windows filename will be ignored, "trimming" only the
characters of the filename and not the extension.

The BASH script `replace` accepts two strings as arguments.  It will replace
the first instance, in each filename in the current directory, of the first
string with the second string.  This is handy for removing spaces from filenames
for use in UNIX/POSIX systems.

## Examples

### trim-front

    $ ls
        001.pdf     002.pdf     003.pdf
    $ trim-front 3
        1.pdf   2.pdf   3.pdf

### trim-end

    $ ls
        a11.jpg     b22.jpg     c33.jpg
    $ trim-end 2
        a.jpg   b.jpg   c.jpg

### replace

    $ ls
        'spaced filename.tif'   regular_document.pdf    'another spaced filename.tif'
    $ replace " " "_"
        spaced_filename.tif     regular_document.pdf    'another_spaced filename.tif'

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_trim-filename.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/{trim-end,trim-front}

## Bugs

The script `trim-front` must have an integer of +1.  So, if you want to trim two
characters from the name, you must use `trim-front 3`, as shown in the Examples
section.

The script `replace` does not work in OSX, as `rename` is not a standard
function provided by BSD.  Also, it will only replace the first "found" instance
of the first string -- this must be run multipile time to replace all
instances -- see Examples.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_trim-filename/src/5638063b38752af2e1bb3ff9255f2acebc07fc56/LICENSE.txt?at=master) file for
details.

